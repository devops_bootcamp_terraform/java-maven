def buildApp() {
    echo "the build stage for ${NEW_VERSION}"
    echo "${NEXUS_VREDENTAILS}"
    sh 'mvn package'
}
def testApp() {
    sh 'mvn test'
    echo "the test stage"
    params.TOGGLE == true
}
def testVersion() {
   
    if (env.BRANCH_NAME == null) {
        error "BRANCH_NAME environment variable not set"
    }
    else if (env.BRANCH_NAME.startsWith("feature/") || env.BRANCH_NAME == "develop" || env.BRANCH_NAME == "preprod") {
        versionn = "${env.BRANCH_NAME}-SNAPSHOT-${env.BUILD_NUMBER}"
        registry_url = "${env.REGISTRY}/dev"
    } else if (env.BRANCH_NAME == "main") {
        version = "1.0.${env.BUILD_NUMBER}"
        registry_urll = "${env.REGISTRY}/prod"
    } else {
        error "Unsupported branch: ${env.BRANCH_NAME}"
    }
}
def buildimgApp() {
    //sh "docker rmi 3.84.14.243:8083/java-maven:${version}"
    sh "docker build -t ${registry_urll}/${env.APP_NAME}:${versionn} ."
    echo "params.PERSON"
}
def pushApp() {
    withCredentials([
        usernamePassword(credentialsId: 'nexus-cred', usernameVariable: 'USER', passwordVariable: 'PWD')
    ]) {
            sh "echo $PWD | docker login -u $USER --password-stdin ${env.REGISTRY}"
            sh "docker push ${registry_urll}/${env.APP_NAME}:${versionn}"
            echo "params.PERSON"
        }
}
def deployApp() {
    echo "the deploy stage"
}
return this
