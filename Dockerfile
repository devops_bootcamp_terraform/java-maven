From openjdk:8-jre-alpine
EXPOSE 8080
COPY ./target/java-maven-app-*.jar /usr/app
WORKDIR -p /usr/app
CMD java -jar java-maven-app-*.jar
