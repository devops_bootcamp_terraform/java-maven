def gv
pipeline {
    agent any
    tools {
        maven 'name-of maven-in-jenkins-config'
    }
    parameters {
        string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')

        text(name: 'BIOGRAPHY', defaultValue: '', description: 'Enter some information about the person')

        booleanParam(name: 'TOGGLE', defaultValue: true, description: 'Toggle this value')

        choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')

        password(name: 'PASSWORD', defaultValue: 'SECRET', description: 'Enter a password')
    }
    environment {
        NEW_VERSION 
        NEXUS_VREDENTAILS = credentials('id-nexus-repo-cred')
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "you-ip-addr-here:8081"
        GROUPID
        VERSION
        ARTIFACTID
    }

    stages {
        stage("init script") {
            steps {
                script {
                    gv = load "script.groovy"
                }                
            }
        }
        stage('Push to Nexus') {
            steps {
                script {
                    def nexusUrl = 'http://nexus.example.com' // replace with your Nexus URL
                    def nexusCredentialsId = 'nexus-credentials' // replace with the ID of your Nexus credentials in Jenkins

                    def groupId = 'com.example' // replace with your project's group ID
                    def artifactId = 'example-app' // replace with your project's artifact ID

                    def nexusArtifactUploader = NexusArtifactUploader.fromGlobalConfiguration(nexusUrl, nexusCredentialsId)

                    if (BRANCH_NAME.startsWith('feature/') || BRANCH_NAME == 'dev') {
                        def snapshotVersion = "${version}-SNAPSHOT"
                        nexusArtifactUploader.deploy(
                            groupId: groupId,
                            artifactId: artifactId,
                            version: snapshotVersion,
                            artifact: 'target/example-app.jar',
                            repository: 'maven-snapshots'
                        )
                    } else {
                        nexusArtifactUploader.deploy(
                            groupId: groupId,
                            artifactId: artifactId,
                            version: version,
                            artifact: 'target/example-app.jar',
                            repository: 'maven-releases'
                        )
                    }
                }
            }
}

        stage("build") {
            when {
                expression {
                    BRANCH_NAME = "develop" || BRANCH_NAME = "preprod" || BRANCH_NAME = "master"
                }
            }
            steps {
                script {
                    gv.buildApp()
                }
            }
        }
        stage("test") {
            when {
                expression {
                    params.TOGGLE
                }
            }
            steps {
                script {
                    gv.testApp()
                }
            }
        }
        stage("push") {
            steps {
                script {
                    WithCredentials([
                        usernamePassword(credentialsId: 'nexus-cred', usernameVariable: USER, passwordVariable: PWD)
                    ]) {
                        gv.pushApp()
                        }
                }
                
            }
        }
       
        stage("deploy to DEV env") {
            when {
                expression {
                    BRANCH_NAME = "develop"
                }
            }
            steps {
                script {
                    gv.deployApp()
            }
        }
        stage("deploy to QA env") {
            when {
                expression {
                    BRANCH_NAME = "prprod"
                }
            }
            steps {
                script {
                    gv.deployApp()
                }
            }
        }
        stage("deploy to  PRODUCTION env") {
            when {
                expression {
                    BRANCH_NAME = "master"
                }
            }
            steps {
                script {
                    gv.deployApp()
                }
            }
        }
    }
    post {
        always {
            //
        }
        success {
            //
        }
        failure {
            //
        }
    }
    }
}