def gv
pipeline {
    agent any
    tools {
        maven 'name-of maven-in-jenkins-config'
    }
    parameters {
        string(name: 'PERSON', defaultValue: 'Mr Jenkins', description: 'Who should I say hello to?')

        text(name: 'BIOGRAPHY', defaultValue: '', description: 'Enter some information about the person')

        booleanParam(name: 'TOGGLE', defaultValue: true, description: 'Toggle this value')

        choice(name: 'CHOICE', choices: ['One', 'Two', 'Three'], description: 'Pick something')

        password(name: 'PASSWORD', defaultValue: 'SECRET', description: 'Enter a password')
    }
    environment {
        NEW_VERSION 
        NEXUS_VREDENTAILS = credentials('id-nexus-repo-cred')
    }

    stages {
        stage("init script") {
            steps {
                script {
                    gv = load "script.groovy"
                }                
            }
        }
        stage("build") {
            when {
                expression {
                    BRANCH_NAME = "develop" || BRANCH_NAME = "preprod" || BRANCH_NAME = "master"
                }
            }
            steps {
                script {
                    gv.buildApp()
                }
            }
        }
        stage("test") {
            when {
                expression {
                    params.TOGGLE
                }
            }
            steps {
                script {
                    gv.testApp()
                }
            }
        }
        stage("push") {
            steps {
                script {
                    WithCredentials([
                        usernamePassword(credentialsId: 'server-cred-id', usernameVariable: USER, passwordVariable: PWD)
                    ]) {
                        gv.pushApp()
                        }
                }
                
            }
        }
       
        stage("deploy to DEV env") {
            when {
                expression {
                    BRANCH_NAME = "develop"
                }
            }
            steps {
                script {
                    gv.deployApp()
            }
        }
        stage("deploy to QA env") {
            when {
                expression {
                    BRANCH_NAME = "prprod"
                }
            }
            steps {
                script {
                    env.ENV = input message: "select env to deploy", ok: "Done", parameters: [choice(name: 'ENVIRONMENT', choices: ['DEV', 'QA', 'PRODUCTION'], description: 'environment choice')]
                    gv.deployApp()
                    echo "deploy to ${ENV}"
                }
            }
        }
        stage("deploy to  PRODUCTION env") {
            when {
                expression {
                    BRANCH_NAME = "master"
                }
            }
            input {
                message "select env to deploy"
                ok "Done"
                parameters {
                    choice(name: 'BRANCH', choices: ['DEVELOP', 'PREPROD', 'MASTER'], description: 'branch choice')
                    choice(name: 'ENVIRONMENT', choices: ['DEV', 'QA', 'PRODUCTION'], description: 'environment choice')
                }
            }
            steps {
                script {
                    gv.deployApp()
                    echo "branch choose is  ${BRANCH}"
                    echo "deploy to ${ENVIRONMENT}"
                }
            }
        }
    }
    post {
        always {
            //
        }
        success {
            //
        }
        failure {
            //
        }
    }
    }
}